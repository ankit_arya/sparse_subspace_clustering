
sprintf('Reading Data')
%X= dlmread('100k_embeddings.txt');

load('Xdata.mat');
load('idx_k500.mat');
%X= dlmread('tp_embedding.txt');
%[X_key] = textread('tp_key.txt','%s ');

X=X';
%{
%%%%%%%%%%%%%  TEMP %%%%%%%%%%%%%%%%%%%%%%%%%%55


D = 64; %Dimension of ambient space
n = 2; %Number of subspaces
d1 = 1; d2 = 1; %d1 and d2: dimension of subspace 1 and 2
N1 = 50; N2 = 50; %N1 and N2: number of points in subspace 1 and 2
X1 = randn(D,d1) * randn(d1,N1); %Generating N1 points in a d1 dim. subspace
X2 = randn(D,d2) * randn(d2,N2); %Generating N2 points in a d2 dim. subspace
X = [X1 X2];
idx=[1:100];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}



r = 0; %Enter the projection dimension e.g. r = d*n, enter r = 0 to not project
Cst = 0; %Enter 1 to use the additional affine constraint sum(c) == 1
OptM = 'Lasso'; %OptM can be {'L1Perfect','L1Noise','Lasso','L1ED'}
lambda = 0.01; %Regularization parameter in 'Lasso' or the noise level for 'L1Noise'
K=0; % Number of top coefficients to build the similarity graph, enter K=0 for using the whole coefficients

N=size(X,2);
%Xp =X;
% DataProjection(X,r,'NormalProj');
CMat=sparse(N,N);
sprintf('Starting Compuatation')
m=size(idx,2);
tic

parfor_progress(N); 
parfor i=1:N,
	parfor_progress; % Count 
	temp=sparse(N,1);
	indx=idx(i,:);
	indx(find(indx==i))=[];
	c = SparseCoefRecovery(X,Cst,OptM,lambda,i,indx);
    temp(indx) = c(1:m-1);
	temp(i)=0;    
	CMat(:,i)=temp;
end
save('Aff_Cmat.mat','CMat');
	sprintf('Computing Adjaceny')
    CKSym = BuildAdjacency(CMat,K);
   % [Grps , SingVals, LapKernel] = SpectralClustering(CKSym,n);

toc
save('Aff_matrix.mat','CKSym');
