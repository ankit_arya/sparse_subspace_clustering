function [k_neighbours] = parallel_mKNN(X,Y,k)

	closest_indx=[];
	k_neighbours=zeros(size(X,1),k);

	parfor i=1:size(Y,1),
			temp=zeros(1,k);
			D = pdist2(X,Y(i,:));
			[B,indx]=sort(D');   %distances and indexes
			temp=indx(1:k);
	
			k_neighbours(i,:)=temp; %pick closest k
	end
	

