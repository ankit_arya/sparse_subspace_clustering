%--------------------------------------------------------------------------
% This function takes the D x N matrix of N data points and write every
% point as a sparse linear combination of other points.
% Xp: D x N matrix of N data points
% cst: 1 if using the affine constraint sum(c)=1, else 0
% Opt: type of optimization, {'L1Perfect','L1Noisy','Lasso','L1ED'}
% lambda: regularizartion parameter of LASSO, typically between 0.001 and 
% 0.1 or the noise level for 'L1Noise'
% CMat: N x N matrix of coefficients, column i correspond to the sparse
% coefficients of data point in column i of Xp
%--------------------------------------------------------------------------
% Copyright @ Ehsan Elhamifar, 2010
%--------------------------------------------------------------------------


function c = SparseCoefRecovery_mutual(Xp,cst,Opt,lambda,i,indx)

if (nargin < 2)
    cst = 0;
end
if (nargin < 3)
    Opt = 'Lasso';
end
if (nargin < 4)
    lambda = 0.001;
end

D = size(Xp,1);
N = size(Xp,2);

%for i = 1:N
    
    y = Xp(:,i);

	Y=Xp(:,indx);
	t=size(indx,2);
        cvx_begin quiet;
    if ( strcmp(Opt , 'Lasso') )
        cvx_precision high;
        variable c(t,1);
        minimize( norm(c,1) + lambda * norm(Y * c  - y) );
		sum(c) == 1;
        cvx_end;
    end
    
