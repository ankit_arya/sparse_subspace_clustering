X= dlmread('100k_embeddings.txt');
%X= dlmread('tp_embedding.txt');


k=500;
idx= parallel_mKNN(X,X,k);

name_s=sprintf('idx_k%d.mat',k);
save(name_s,'idx');
%{
adj_matrix=zeros(size(X,1),size(X,1));

parfor i=1:size(idx,1),
	temp=zeros(1,size(idx,1)); %slice output variable
	for neigh=1:size(idx,2),
		val=idx(i,neigh); % indx value of the neighbour
		
		if any(ismember(idx(val,:),i)) %check mutual membership
			temp(val)=1;
		end
	end
	adj_matrix(i,:)=temp; %update adjaceny matrix
end

%}
